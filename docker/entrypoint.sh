#!/bin/sh

echo -n "export DATABASE_URL=$DB_CONNECTION://$DB_USERNAME:$DB_PASSWORD@$DB_HOST:$DB_PORT/$DB_DATABASE" > temp &&
source temp &&
rm temp &&

echo -n "$KAFKA_CA_AUTHORITY" > /certs/kafka-ca.crt &&

/wait

if [ $? -ne 0 ]; then
    echo "Execution of /wait failed."
    exit 1
fi

if [[ ! -z "${RUN_MIGRATIONS}" ]]; then
  npm run migrate
fi

$CLI_TO_RUN