import http from "http";
import {connection, request, server as WebSocketServer} from "websocket";
import {PrismaClient} from "@prisma/client";
import jp from "jsonpath";

import producer from "./services/kafka/producer.js";
import {
    fetchDeviceWithSensors
} from "./repositories/device.js";
import {verifyApiKey} from "./services/apiKey/verifier.js";
import {markDeviceConnected, markDeviceDisconnected} from "./services/redis/devices.js";
import {consumeDeviceAction} from "./services/redis/deviceActions.js";

const redisTTL = 60;
const redisRefreshRate = 50;
const poolActionsEveryMs = 100;

const podName = process.env.POD_NAME ?? '';
const appUrl = process.env.APP_URL ?? '';
const deviceSensorTopic = process.env.KAFKA_DEVICE_SENSOR_TOPIC ?? '';

type DeviceDataSensor = {
    uuid: string,
    query: string
}

type OpenConnection = {
    connection: connection,
    sensors: DeviceDataSensor[]
};

const openConnections: Record<string, OpenConnection> = {};

console.log("Connecting to MySQL...");
const prisma = new PrismaClient();

function initHttpServer<
    Request extends typeof http.IncomingMessage = typeof http.IncomingMessage,
    Response extends typeof http.ServerResponse = typeof http.ServerResponse,
>() {
    const requestListener: http.RequestListener<Request, Response> = (request, response) => {
        console.log("Received request for " + request.url);
        response.writeHead(404);
        response.end();
    };

    return http.createServer(requestListener);
}

/**
 * Just a function that updates the device keys in redis, so that they do not expire.
 */
async function updateConnectedDevices() {
    const uuids = Object.keys(openConnections);
    if (uuids.length === 0)
        return;

    console.log("Updating connected device data in redis...");

    uuids.forEach(uuid => markDeviceConnected(uuid, podName, {
        ttl: redisTTL,
        overwrite: true
    }));

    console.log("Updating connected device data in redis finished!");
}

/**
 * Just a function that pools pending connected device actions from redis.
 */
async function poolDeviceActions() {
    const uuids = Object.keys(openConnections);
    if (uuids.length === 0)
        return;

    let payload = await consumeDeviceAction(podName);
    while (payload !== null) {
        const {action, deviceUuid} = payload;

        console.log(`Received action ${action} for device ${deviceUuid}!`);

        if (!(deviceUuid in openConnections)) {
            console.log(`WARN: Device ${deviceUuid} is not in open connections, skipping...`);
            payload = await consumeDeviceAction(podName);
            continue;
        }

        switch (action) {
            case "disconnect":
                console.log(`Disconnecting device: ${deviceUuid}...`);
                openConnections[deviceUuid].connection.close(1002, "Disconnected by device action!");
                break;
            case "configure":
                console.log(`Reconfiguring device: ${deviceUuid}...`);
                sendConfigResponse(openConnections[deviceUuid].connection, payload.config);
                break;
        }

        payload = await consumeDeviceAction(podName);
    }
}

async function main() {
    setInterval(() => updateConnectedDevices(), redisRefreshRate * 1000);
    setInterval(() => poolDeviceActions(), poolActionsEveryMs);

    console.log("Creating http server...");
    const httpServer = initHttpServer();

    const PORT = process.env.PORT;
    httpServer.listen(PORT, () => {
        console.log(`Server is listening on port ${PORT}`);
    });

    const wsServer = new WebSocketServer({
        httpServer: httpServer,
        autoAcceptConnections: false
    });

    wsServer.on('request', handleRequest);
}

type DeviceCredentials = {
    uuid: string | null,
    api_key: string | null
}

function findDeviceCredentialsFromRequest(request: request): DeviceCredentials {
    const urlString = request.httpRequest.url;
    if (urlString === undefined)
        return {
            uuid: null,
            api_key: null
        };

    const url = new URL(appUrl + urlString);
    return {
        uuid: url.searchParams.get('uuid'),
        api_key: url.searchParams.get('apiKey'),
    }
}

function logRejectedRequest(origin: string, reason: string): void {
    console.log(`Connection from origin ${origin} rejected, ${reason}`);
}

async function handleRequest(request: request) {
    const clientIp = request.socket.remoteAddress;
    console.log(`Device attempts to connect from ${clientIp}...`);

    const {uuid, api_key} = findDeviceCredentialsFromRequest(request);

    if (uuid === null) {
        logRejectedRequest(request.origin, "device did not specify uuid!");
        request.reject();
        return;
    }

    if (api_key === null) {
        logRejectedRequest(request.origin, "device did not specify apiKey!");
        request.reject();
        return;
    }

    const device = await fetchDeviceWithSensors(prisma, uuid);
    if (device === null) {
        logRejectedRequest(request.origin, `device specified not existing uuid: ${uuid}!`);
        request.reject();
        return;
    }

    if (!await verifyApiKey(api_key, device.apiKeyHash)) {
        logRejectedRequest(request.origin, `device specified invalid api key: ${api_key} (uuid: ${uuid})!`);
        request.reject();
        return;
    }


    if (!await markDeviceConnected(uuid, podName, {
        ttl: redisTTL
    })) {
        logRejectedRequest(request.origin, `device authed successfully, but is already connected (uuid: ${uuid})!`);
        request.reject();
        return;
    }

    const connection = request.accept('', request.origin);

    openConnections[uuid] = {
        connection: connection,
        sensors: device.sensors.map(sensor => ({
            uuid: sensor.uuid,
            query: sensor.query
        }))
    };

    sendConfigResponse(connection, device.config);

    handleAcceptedConnection(connection, uuid);
}

function handleAcceptedConnection(connection: connection, uuid: string) {
    console.log(`Connection accepted | Device uuid: ${uuid}`);

    connection.on('message', (message) => {
        if (message.type === 'binary') {
            console.log(`Connection from device ${uuid} closing, because it send binary message.`);
            connection.close(1002, "Binary messages are not supported!");
            return;
        }

        if (message.type === 'utf8') {
            console.log(`Received message from device ${uuid}.`);
            console.log(`Parsing message json.`)
            let inputData: string | null = null;
            try {
                inputData = JSON.parse(message.utf8Data);
            } catch (e) {
                console.log("Device didn't send a valid json!")
                connection.close(1002, "Sent message was not a JSON!");
                return;
            }

            console.log("Mapping sensor data...")
            const sensorData: Record<string, any> = {};
            openConnections[uuid].sensors.forEach((sensor: DeviceDataSensor) => {
                try {
                    sensorData[sensor.uuid] = jp.query(inputData, sensor.query)
                } catch (e) {
                    console.log("Sensor " + sensor.uuid + " jsonpath is broken: " + sensor.query);
                    sensorData[sensor.uuid] = null;
                }
            });

            console.log(`Pushing sensor data for device ${uuid}.`);
            producer.send({
                topic: deviceSensorTopic,
                messages: [{
                    key: JSON.stringify({
                        deviceKey: uuid,
                        date: new Date().toISOString()
                    }),
                    value: JSON.stringify(sensorData)
                }]
            }).then(() => sendOkResponse(connection));
        }
    });

    connection.on('close', async (reasonCode, description) => {
        console.log(`Connection from device ${uuid} closed. Reason code: ${reasonCode}, Description ${description}`);
        await markDeviceDisconnected(uuid)
        delete openConnections[uuid];
    });
}

function sendConfigResponse(connection: connection, config: any) {
    connection.sendUTF(JSON.stringify({
        type: "config",
        config: config
    }));
}

function sendOkResponse(connection: connection) {
    connection.sendUTF(JSON.stringify({
        type: "ok"
    }));
}

main()
    .then(async () => {
        await prisma.$disconnect();
    })
    .catch(async (e) => {
        console.error(e);
        await prisma.$disconnect();
        process.exit(1);
    });