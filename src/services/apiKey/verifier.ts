import argon2 from "argon2";

export async function verifyApiKey(
    apiKey: string,
    apiKeyHash: string
): Promise<boolean> {
    return await argon2.verify(apiKeyHash, apiKey);
}