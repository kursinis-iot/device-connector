import {Kafka, KafkaConfig} from "kafkajs";
import fs from "fs";

console.log("Connecting to Kafka...");
const config: KafkaConfig = {
    clientId: process.env.KAFKA_CLIENT_ID,
    brokers: process.env.KAFKA_BROKERS?.split(',') ?? [],
};

if (process.env.KAFKA_SECURITY_PROTOCOL === "SSL") {
    config.ssl = {
        rejectUnauthorized: true,
        ca: [fs.readFileSync('/certs/kafka-ca.crt')]
    }
}

const kafka = new Kafka(config);

export default kafka;