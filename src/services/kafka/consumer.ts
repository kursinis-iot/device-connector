import kafka from "./kafka.js";

const consumer = kafka.consumer({groupId: process.env.KAFKA_GROUP_ID ?? 'default'});
await consumer.connect();

export default consumer;