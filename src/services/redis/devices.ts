import client, {prefix} from "./client.js";

export function constructDeviceKey(uuid: string): string {
    return `${prefix}.devices.${uuid}`;
}

export type MarkDeviceConnectedOptions = {
    overwrite?: boolean
    ttl?: number
}

export async function markDeviceConnected(
    uuid: string,
    podName: string,
    options: MarkDeviceConnectedOptions = {}
): Promise<boolean> {
    const overwrite = options.overwrite ?? false;
    const ttl = options.ttl ?? 60;

    const response = await client.set(
        constructDeviceKey(uuid),
        podName,
        {
            EX: ttl,
            NX: !overwrite
        }
    );

    return response === "OK";
}

export async function markDeviceDisconnected(
    uuid: string
): Promise<void> {
    await client.del(constructDeviceKey(uuid));
}

export async function isDeviceConnected(
    uuid: string
): Promise<boolean> {
    return await client.exists(constructDeviceKey(uuid)) === 1;
}

export async function getDevicePodName(
    uuid: string
): Promise<string | null> {
    return await client.get(constructDeviceKey(uuid));
}