import client, {prefix} from "./client.js";

export type ConfigureAction = {
    action: "configure",
    deviceUuid: string
    config: any
};

export type DisconnectAction = {
    action: "disconnect",
    deviceUuid: string
}

export type DeviceAction = ConfigureAction | DisconnectAction;

export function constructDeviceActionQueueKey(pod: string): string {
    return `${prefix}.actions.${pod}`;
}

export async function dispatchDeviceAction(
    pod: string,
    action: DeviceAction
): Promise<void> {
    await client.rPush(
        constructDeviceActionQueueKey(pod),
        JSON.stringify(action)
    );
}

export async function consumeDeviceAction(
    pod: string
): Promise<DeviceAction | null> {
    const val = await client.lPop(constructDeviceActionQueueKey(pod));
    if (val === null)
        return null;

    return JSON.parse(val);
}