import {Prisma, PrismaClient} from "@prisma/client";

const deviceWithSensors = Prisma.validator<Prisma.DeviceDefaultArgs>()({
    include: {sensors: true},
})
type DeviceWithSensors = Prisma.DeviceGetPayload<typeof deviceWithSensors>

export function fetchDeviceWithSensors(
    prisma: PrismaClient,
    uuid: string
): Promise<DeviceWithSensors | null> {
    return prisma.device.findFirst({
        where: {
            uuid: uuid
        },
        include: {
            sensors: true
        }
    });
}

export type Sensor = {
    uuid: string,
    query: string,
};

export async function upsertDevice(
    prisma: PrismaClient,
    uuid: string,
    config: object,
    apiKeyHash: string,
    sensors: Sensor[]
) {
    await prisma.$transaction(async (prisma) => {
        await prisma.device.upsert({
            where: {
                uuid: uuid
            },
            update: {
                config: config,
                apiKeyHash: apiKeyHash,
            },
            create: {
                uuid: uuid,
                config: config,
                apiKeyHash: apiKeyHash,
            }
        });

        await prisma.deviceSensor.deleteMany({
            where: {deviceUuid: uuid}
        });

        await prisma.deviceSensor.createMany({
            data: sensors.map(sensor => ({...sensor, deviceUuid: uuid}))
        });
    });
}


export async function deleteDevice(prisma: PrismaClient, uuid: string) {
    await prisma.device.delete({
        where: {
            uuid: uuid
        },
    });
}