import {PrismaClient} from "@prisma/client";
import {EachMessagePayload} from "kafkajs";

import consumer from "../services/kafka/consumer.js";
import {deleteDevice, upsertDevice} from "../repositories/device.js";
import {getDevicePodName} from "../services/redis/devices.js";
import {dispatchDeviceAction} from "../services/redis/deviceActions.js";

console.log("Connecting to MySQL...");
const prisma = new PrismaClient();

//Topic names
const devicesTopic = process.env.KAFKA_DEVICE_TOPIC ?? '';

export type DeviceSensor = {
    uuid: string,
    created_at: string,
    updated_at: string,
    device_uuid: string,
    jsonpath_query: string
}

export type NeededAction = "sync" | "reconfigure" | "disconnect";
export type DeviceMessagePayload = {
    uuid: string,
    config: string,
    created_at: string,
    updated_at: string,
    api_key_hash: string,
    needed_action: NeededAction
    sensors: DeviceSensor[]
}

async function main() {
    await consumer.subscribe({topic: devicesTopic, fromBeginning: true});

    console.log("Starting consumer...");
    await consumer.run({
        eachMessage: async ({message: {offset, key, value}}: EachMessagePayload) => {
            console.log(`Consumed offset: ${offset}...`);

            if (key === null) {
                console.log(`Key is null, skipping...`);
                return;
            }

            const deviceUuid = key.toString();
            if (value === null) {
                console.log(`Removing ${deviceUuid}...`);
                await deleteDevice(prisma, deviceUuid);

                const pod = await getDevicePodName(deviceUuid);
                if (pod !== null) {
                    console.log(`${deviceUuid} is connected to ${pod}, dispatching disconnect action...`);
                    await dispatchDeviceAction(pod, {
                        action: "disconnect",
                        deviceUuid: deviceUuid
                    });
                }

                return;
            }

            console.log(`Upserting ${deviceUuid}...`);
            const payload: DeviceMessagePayload = JSON.parse(value.toString()) as any;
            const config = JSON.parse(payload.config);
            await upsertDevice(
                prisma,
                deviceUuid,
                config,
                payload.api_key_hash,
                payload.sensors.map((sensor) => ({
                    uuid: sensor.uuid,
                    query: sensor.jsonpath_query,
                }))
            );

            if (payload.needed_action === "sync")
                return;

            const pod = await getDevicePodName(deviceUuid);
            if (pod === null)
                return;

            switch (payload.needed_action) {
                case "disconnect":
                    console.log(`${deviceUuid} is connected to ${pod} and needed_action="disconnect", dispatching disconnect action...`);
                    await dispatchDeviceAction(pod, {action: "disconnect", deviceUuid: deviceUuid});
                    return;
                case "reconfigure":
                    console.log(`${deviceUuid} is connected to ${pod} and needed_action="reconfigure", dispatching configure action...`);
                    await dispatchDeviceAction(pod, {action: "configure", deviceUuid: deviceUuid, config: config});
                    return;
            }
        }
    });
}

main()
    .then(async () => {
        await prisma.$disconnect();
    })
    .catch(async (e) => {
        console.error(e);
        await prisma.$disconnect();
        process.exit(1);
    });