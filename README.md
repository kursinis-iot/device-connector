# Device Connector

## Description

This is a Javascript application that accepts WebSocket connections from IoT devices.
It produces streamed data from IoT devices to kafka. 
Moreover, it also consumes configuration changes and redirects them to connected devices.