/*
  Warnings:

  - You are about to alter the column `apiKeyHash` on the `Device` table. The data in that column could be lost. The data in that column will be cast from `VarChar(256)` to `VarChar(255)`.
  - You are about to alter the column `query` on the `DeviceSensor` table. The data in that column could be lost. The data in that column will be cast from `VarChar(256)` to `VarChar(255)`.

*/
-- AlterTable
ALTER TABLE `Device` MODIFY `apiKeyHash` VARCHAR(255) NOT NULL;

-- AlterTable
ALTER TABLE `DeviceSensor` MODIFY `query` VARCHAR(255) NOT NULL;
