-- CreateTable
CREATE TABLE `Device` (
    `deviceKey` CHAR(36) NOT NULL,
    `config` JSON NOT NULL,
    `kafkaSensorPartition` INTEGER UNSIGNED NOT NULL,
    `authorUuid` CHAR(36) NOT NULL,
    `createdAt` DATETIME(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
    `updatedAt` DATETIME(3) NOT NULL,

    UNIQUE INDEX `Device_kafkaSensorPartition_key`(`kafkaSensorPartition`),
    PRIMARY KEY (`deviceKey`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `DeviceSensor` (
    `uuid` CHAR(36) NOT NULL,
    `deviceKey` CHAR(36) NOT NULL,
    `query` VARCHAR(256) NOT NULL,
    `createdAt` DATETIME(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
    `updatedAt` DATETIME(3) NOT NULL,

    PRIMARY KEY (`uuid`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `UnallocatedPartition` (
    `partition` INTEGER UNSIGNED NOT NULL,

    PRIMARY KEY (`partition`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `Param` (
    `key` VARCHAR(191) NOT NULL,
    `value` VARCHAR(191) NOT NULL,

    PRIMARY KEY (`key`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- AddForeignKey
ALTER TABLE `DeviceSensor` ADD CONSTRAINT `DeviceSensor_deviceKey_fkey` FOREIGN KEY (`deviceKey`) REFERENCES `Device`(`deviceKey`) ON DELETE RESTRICT ON UPDATE CASCADE;
