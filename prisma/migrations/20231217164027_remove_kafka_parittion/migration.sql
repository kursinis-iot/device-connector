/*
  Warnings:

  - You are about to drop the column `kafkaSensorPartition` on the `Device` table. All the data in the column will be lost.
  - You are about to drop the `Param` table. If the table is not empty, all the data it contains will be lost.
  - You are about to drop the `UnallocatedPartition` table. If the table is not empty, all the data it contains will be lost.

*/
-- DropIndex
DROP INDEX `Device_kafkaSensorPartition_key` ON `Device`;

-- AlterTable
ALTER TABLE `Device` DROP COLUMN `kafkaSensorPartition`;

-- DropTable
DROP TABLE `Param`;

-- DropTable
DROP TABLE `UnallocatedPartition`;
