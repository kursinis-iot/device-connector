/*
  Warnings:

  - The primary key for the `Device` table will be changed. If it partially fails, the table could be left without primary key constraint.
  - You are about to drop the column `connected` on the `Device` table. All the data in the column will be lost.
  - You are about to drop the column `deviceKey` on the `Device` table. All the data in the column will be lost.
  - You are about to drop the column `deviceKey` on the `DeviceSensor` table. All the data in the column will be lost.
  - Added the required column `apiKeyHash` to the `Device` table without a default value. This is not possible if the table is not empty.
  - Added the required column `uuid` to the `Device` table without a default value. This is not possible if the table is not empty.
  - Added the required column `deviceUuid` to the `DeviceSensor` table without a default value. This is not possible if the table is not empty.

*/
-- DropForeignKey
ALTER TABLE `DeviceSensor` DROP FOREIGN KEY `DeviceSensor_deviceKey_fkey`;

-- AlterTable
ALTER TABLE `Device` DROP PRIMARY KEY,
    DROP COLUMN `connected`,
    DROP COLUMN `deviceKey`,
    ADD COLUMN `apiKeyHash` VARCHAR(256) NOT NULL,
    ADD COLUMN `uuid` CHAR(36) NOT NULL,
    ADD PRIMARY KEY (`uuid`);

-- AlterTable
ALTER TABLE `DeviceSensor` DROP COLUMN `deviceKey`,
    ADD COLUMN `deviceUuid` CHAR(36) NOT NULL;

-- AddForeignKey
ALTER TABLE `DeviceSensor` ADD CONSTRAINT `DeviceSensor_deviceUuid_fkey` FOREIGN KEY (`deviceUuid`) REFERENCES `Device`(`uuid`) ON DELETE CASCADE ON UPDATE CASCADE;
