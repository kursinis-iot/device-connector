-- DropForeignKey
ALTER TABLE `DeviceSensor` DROP FOREIGN KEY `DeviceSensor_deviceKey_fkey`;

-- AddForeignKey
ALTER TABLE `DeviceSensor` ADD CONSTRAINT `DeviceSensor_deviceKey_fkey` FOREIGN KEY (`deviceKey`) REFERENCES `Device`(`deviceKey`) ON DELETE CASCADE ON UPDATE CASCADE;
