import client from "../../../src/services/redis/client";
import {
    ConfigureAction,
    consumeDeviceAction,
    DisconnectAction,
    dispatchDeviceAction
} from "../../../src/services/redis/deviceActions";

afterEach(async () => {
    await client.flushAll();
});

afterAll(async () => {
    await client.disconnect()
});

const configureAction: ConfigureAction = {
    action: "configure",
    deviceUuid: "test1",
    config: {lamps: [true, false]}
};

const disconnectAction: DisconnectAction = {
    action: "disconnect",
    deviceUuid: "test2"
};

test("integration test", async () => {
    expect(await consumeDeviceAction("a")).toBeNull();
    expect(await consumeDeviceAction("b")).toBeNull();

    await dispatchDeviceAction("a", configureAction);
    expect(await consumeDeviceAction("a")).toEqual(configureAction);
    expect(await consumeDeviceAction("a")).toBeNull();
    expect(await consumeDeviceAction("b")).toBeNull();

    await dispatchDeviceAction("a", configureAction);
    await dispatchDeviceAction("a", disconnectAction);
    await dispatchDeviceAction("b", configureAction);
    expect(await consumeDeviceAction("a")).toEqual(configureAction);
    expect(await consumeDeviceAction("a")).toEqual(disconnectAction);
    expect(await consumeDeviceAction("a")).toBeNull();
    expect(await consumeDeviceAction("b")).toEqual(configureAction);
    expect(await consumeDeviceAction("b")).toBeNull();
});