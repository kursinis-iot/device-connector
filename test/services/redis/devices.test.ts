import client from "../../../src/services/redis/client";
import {
    constructDeviceKey, getDevicePodName,
    isDeviceConnected,
    markDeviceConnected,
    markDeviceDisconnected
} from "../../../src/services/redis/devices";

afterEach(async () => {
    await client.flushAll();
});

afterAll(async () => {
    await client.disconnect()
});

test("markDeviceConnected: test default parameters", async () => {
    expect(await markDeviceConnected("test", "my-pod", {})).toBe(true);

    const key = constructDeviceKey("test");
    expect(await client.get(key)).toEqual("my-pod");
    expect(await client.ttl(key)).toBe(60);

    expect(await markDeviceConnected("test", "my-pod", {})).toBe(false);
});

test("markDeviceConnected: test custom TTL", async () => {
    expect(await markDeviceConnected("test", "my-pod", {ttl: 120})).toBe(true);

    const key = constructDeviceKey("test");
    expect(await client.get(key)).toEqual("my-pod");
    expect(await client.ttl(key)).toBe(120);

    expect(await markDeviceConnected("test", "my-pod", {})).toBe(false);
});

test("markDeviceConnected: test overwrite TTL", async () => {
    expect(await markDeviceConnected("test", "my-pod")).toBe(true);
    expect(await markDeviceConnected("test", "my-pod-2", {overwrite: true})).toBe(true);

    const key = constructDeviceKey("test");
    expect(await client.get(key)).toEqual("my-pod-2");
    expect(await client.ttl(key)).toBe(60);
});

test("markDeviceDisconnected: tests if deletes key", async () => {
    expect(await markDeviceConnected("test", "my-pod")).toBe(true);

    await markDeviceDisconnected("test");

    const key = constructDeviceKey("test");
    expect(await client.get(key)).toBeNull();
});

test("isDeviceConnected", async () => {
    expect(await isDeviceConnected("test")).toBe(false);

    expect(await markDeviceConnected("test", "my-pod")).toBe(true);
    expect(await isDeviceConnected("test")).toBe(true);

    await markDeviceDisconnected("test");
    expect(await isDeviceConnected("test")).toBe(false);
});

test("getDevicePodName", async () => {
    expect(await getDevicePodName("test")).toBe(null);

    expect(await markDeviceConnected("test", "my-pod")).toBe(true);
    expect(await getDevicePodName("test")).toBe("my-pod");

    await markDeviceDisconnected("test");
    expect(await getDevicePodName("test")).toBe(null);
});