import {verifyApiKey} from "../../../src/services/apiKey/verifier";

test("verifyApiKey: random device from staging", async () => {
    const apiKeyHash = "$argon2id$v=19$m=65536,t=4,p=1$UkFyaE93VExOcEJHeUdoZw$42gGUJ4uCSANOYen1vJe7MkhBnOtzNgJbacwYvA+TPE";

    expect(await verifyApiKey("9ae3d147-db51-4bab-9783-5370547f90cf", apiKeyHash)).toBe(true);
    expect(await verifyApiKey("9ae3d147-db51-4bab-9783-5370547f90cd", apiKeyHash)).toBe(false);
    expect(await verifyApiKey("something-total-different", apiKeyHash)).toBe(false);
})