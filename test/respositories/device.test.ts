import {deleteDevice, upsertDevice} from "../../src/repositories/device";
import {PrismaClient} from "@prisma/client";

const prisma = new PrismaClient();

beforeAll(async () => {
    await prisma.device.deleteMany();
})

afterEach(async () => {
    await prisma.device.deleteMany();
})

afterAll(async () => {
    await prisma.$disconnect();
})

test('upsertDevice: random device insert (no sensors) (insert)', async () => {
    await upsertDevice(
        prisma,
        "test",
        {},
        'i_am_api_key',
        []
    );

    expect(await prisma.device.count()).toBe(1);
    expect(await prisma.device.findFirst({
        where: {uuid: "test"},
        select: {uuid: true, config: true, apiKeyHash: true}
    })).toEqual({
        apiKeyHash: "i_am_api_key",
        config: {},
        uuid: "test"
    })
});

test('upsertDevice: random device (two sensors) (insert)', async () => {
    await upsertDevice(
        prisma,
        "test",
        {},
        'i_am_api_key',
        [
            {
                uuid: "test1",
                query: "query1",
            },
            {
                uuid: "test2",
                query: "query2",
            }
        ]
    );

    expect(await prisma.device.count()).toBe(1);
    expect(await prisma.device.findFirst({
        where: {uuid: "test"},
        select: {uuid: true, config: true, apiKeyHash: true}
    })).toEqual({
        apiKeyHash: "i_am_api_key",
        config: {},
        uuid: "test"
    })

    expect(await prisma.deviceSensor.count()).toBe(2);
    expect(await prisma.deviceSensor.findFirst({
        where: {uuid: "test1"},
        select: {uuid: true, query: true, deviceUuid: true}
    })).toEqual({
        uuid: "test1",
        query: "query1",
        deviceUuid: "test",
    });
    expect(await prisma.deviceSensor.findFirst({
        where: {uuid: "test2"},
        select: {uuid: true, query: true, deviceUuid: true}
    })).toEqual({
        uuid: "test2",
        query: "query2",
        deviceUuid: "test"
    })
});

test('upsertDevice: random device (no sensors) (update)', async () => {
    prisma.device.create({
        data: {
            uuid: "test",
            apiKeyHash: "somethingElse",
            config: "somethingElse",
        }
    });

    await upsertDevice(
        prisma,
        "test",
        {},
        'i_am_api_key',
        []
    );

    expect(await prisma.device.count()).toBe(1);
    expect(await prisma.device.findFirst({
        where: {uuid: "test"},
        select: {uuid: true, config: true, apiKeyHash: true}
    })).toEqual({
        apiKeyHash: "i_am_api_key",
        config: {},
        uuid: "test"
    })

    expect(await prisma.deviceSensor.count()).toBe(0);
});

test('upsertDevice: random device (update add sensor)', async () => {
    await prisma.device.create({
        data: {
            uuid: "test",
            apiKeyHash: "i_am_api_key",
            config: {},
            sensors: {
                create: {
                    uuid: "test1",
                    query: "test"
                }
            }
        }
    });

    expect(await prisma.deviceSensor.count()).toBe(1);

    await upsertDevice(
        prisma,
        "test",
        {},
        'i_am_api_key',
        [
            {
                uuid: "test1",
                query: "query1",
            },
            {
                uuid: "test2",
                query: "query2",
            }
        ]
    );

    expect(await prisma.device.count()).toBe(1);
    expect(await prisma.device.findFirst({
        where: {uuid: "test"},
        select: {uuid: true, config: true, apiKeyHash: true}
    })).toEqual({
        apiKeyHash: "i_am_api_key",
        config: {},
        uuid: "test"
    })

    expect(await prisma.deviceSensor.count()).toBe(2);
    expect(await prisma.deviceSensor.findFirst({
        where: {uuid: "test1"},
        select: {uuid: true, query: true, deviceUuid: true}
    })).toEqual({
        uuid: "test1",
        query: "query1",
        deviceUuid: "test",
    });
    expect(await prisma.deviceSensor.findFirst({
        where: {uuid: "test2"},
        select: {uuid: true, query: true, deviceUuid: true}
    })).toEqual({
        uuid: "test2",
        query: "query2",
        deviceUuid: "test"
    })
});

test('upsertDevice: random device (update delete sensor)', async () => {
    await prisma.device.create({
        data: {
            uuid: "test",
            apiKeyHash: "i_am_api_key",
            config: {},
            sensors: {
                create: [
                    {
                        uuid: "test1",
                        query: "test"
                    },
                    {
                        uuid: "test2",
                        query: "test"
                    },
                    {
                        uuid: "test3",
                        query: "test"
                    }
                ]
            }
        }
    });

    expect(await prisma.deviceSensor.count()).toBe(3);

    await upsertDevice(
        prisma,
        "test",
        {},
        'i_am_api_key',
        [
            {
                uuid: "test1",
                query: "query1",
            },
            {
                uuid: "test2",
                query: "query2",
            }
        ]
    );

    expect(await prisma.device.count()).toBe(1);
    expect(await prisma.device.findFirst({
        where: {uuid: "test"},
        select: {uuid: true, config: true, apiKeyHash: true}
    })).toEqual({
        apiKeyHash: "i_am_api_key",
        config: {},
        uuid: "test"
    })

    expect(await prisma.deviceSensor.count()).toBe(2);
    expect(await prisma.deviceSensor.findFirst({
        where: {uuid: "test1"},
        select: {uuid: true, query: true, deviceUuid: true}
    })).toEqual({
        uuid: "test1",
        query: "query1",
        deviceUuid: "test",
    });
    expect(await prisma.deviceSensor.findFirst({
        where: {uuid: "test2"},
        select: {uuid: true, query: true, deviceUuid: true}
    })).toEqual({
        uuid: "test2",
        query: "query2",
        deviceUuid: "test"
    })
});

test("deleteDevice: should delete only the provided uuid", async () => {
    await prisma.device.create({
        data: {
            uuid: "test",
            apiKeyHash: "i_am_api_key",
            config: {},
            sensors: {
                create: [
                    {
                        uuid: "test1",
                        query: "test"
                    },
                    {
                        uuid: "test2",
                        query: "test"
                    },
                    {
                        uuid: "test3",
                        query: "test"
                    }
                ]
            }
        }
    });

    await prisma.device.create({
        data: {
            uuid: "test2",
            apiKeyHash: "i_am_api_key",
            config: {},
            sensors: {
                create: [
                    {
                        uuid: "test21",
                        query: "test"
                    },
                    {
                        uuid: "test22",
                        query: "test"
                    },
                    {
                        uuid: "test23",
                        query: "test"
                    }
                ]
            }
        }
    });

    expect(await prisma.device.count()).toBe(2);
    expect(await prisma.deviceSensor.count()).toBe(6);

    await deleteDevice(prisma, "test2");

    expect(await prisma.device.count()).toBe(1);
    expect(await prisma.deviceSensor.count()).toBe(3);

    expect(await prisma.device.findFirst({where: {uuid: "test2"}})).toBeNull();
    expect(await prisma.deviceSensor.findFirst({where: {uuid: "test21"}})).toBeNull();
    expect(await prisma.deviceSensor.findFirst({where: {uuid: "test22"}})).toBeNull();
    expect(await prisma.deviceSensor.findFirst({where: {uuid: "test23"}})).toBeNull();
});